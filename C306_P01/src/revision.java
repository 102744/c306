import java.util.ArrayList;

public class revision {
	public static void main (String args[]){
		//1.2.1 Ex: Variable and Types
		System.out.println("//Varibale and Types//");
		//Part 1
		System.out.println("~~`Part 1`~~");
		String country = "SG";
		System.out.println(country);
		//Part 2
		System.out.println("~~`Part 2`~~");
		int age = 25;
		System.out.println(age);
		//Part 3
		System.out.println("~~`Part 3`~~");
		double price = 10.0 *0.5;
		System.out.println(price);
		//Part 4
		System.out.println("~~`Part 4`~~");
		String total = "25" + "5";
		System.out.println(total);

		//1.3.1.1 Ex: Arrays
		System.out.println("//Arrays//");
		//Part 1
		System.out.println("~~`Part 1`~~");
		String names[] = {"john", "paul", "george", "pete"};
		//Part 2
		System.out.println("~~`Part 2`~~");
		System.out.println(names[0]); 
		//Part 3
		System.out.println("~~`Part 3`~~");
		names[3] = "ringo";

		/* 1.3.3 Ex: Combined variable and arrays
	 	System.out.println("//Combined variable and arrays//");
		int a[] = {10,20,30};
		int b[] = {50,60,70};
		int c = a[0] + b[0];
		System.out.println(c); //60
		 */
		//1.4.1 Ex: Logical Operators
		System.out.println("//Logical Operators//");
		String a = "apple";
		String b = "orange";
		//Part 1
		System.out.println("~~`Part 1`~~");
		System.out.println(a == b || a == "apple"); //true
		//Part 2
		System.out.println("~~`Part 2`~~");
		System.out.println(a != b); //true
		/*Part 3
		System.out.println("~~`Part 3`~~");
		System.out.println(a[4] == b[5] && a[0] == b[2]); // error
		 */
		//NOTE!!**
		if (a.charAt(4)== b.charAt(5)){
			
		}

		//1.5.3 Ex: loop
		System.out.println("//Loop//");
		//Part 1
		System.out.println("~~`Part 1`~~");
		String fruits[] = {"apple","orange","apricot","avocado", "banana"};
		for (int i =0; i < fruits.length;i++){
			System.out.println(fruits[i]);
		}
		//Part 2
		System.out.println("~~`Part 2`~~");
		ArrayList<String>fruit = new ArrayList<String>();
		fruit.add("apple");
		fruit.add("orange");
		fruit.add("apricot");
		fruit.add("avocado");
		fruit.add("banana");
		
		for(int i = 0; i <fruit.size();i++){
			System.out.println(fruit.get(i));
		}

		//1.6.1 Ex: Conditional
		System.out.println("//Conditional//");
		//Part 1
		System.out.println("~~`Part 1`~~");
		String vehicle = "plane";
		//	String vehicle = "train";
		//	String vehicle = "automobile";

		if(vehicle.equals("plane")){
			System.out.println("It can fly");
		}

		//1.6.2 Ex: Conditional and Loop
		System.out.println("//Conditional and Loop//");
		//Part 1
		System.out.println("~~`Part 1`~~");
		//Using above ArrayList (refer to 1.5.3 Ex: loop; part 2)
		for(int i = 0; i <fruit.size();i++){
			if (fruit.get(i).length() == 6){
				System.out.println(fruit.get(i));
			}
		}
		//1.10.2 Ex: Method 1
		System.out.println("//Method 1//");

		echo("woohoo ");
		System.out.println();
		echo("hello ");
		System.out.println();
	
		//1.10.3 Ex: Method 2
		System.out.println("//Method 2//");
		
		echo("woohoo ",2);
		System.out.println();
		echo("yo ",4);

	}
	//1.10.2 Ex: Method 1
	public static void echo(String words){
		for(int i = 0; i < 3; i++){
			System.out.print(words);
		}
	}
	
	//1.10.3 Ex: Method 2
	public static void echo(String word, int n){
		
		for(int i = 0; i < n; i++){
			System.out.print(word);
		}
	}
}
