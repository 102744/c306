import java.util.ArrayList;

public class ForLoopDemo {

  public static void main(String [] args) {
    String [] nameArray = {"John", "May", "Paul", "Peter"};
    
    ArrayList <String> nameList = new ArrayList <String> ();
    nameList.add("John");
    nameList.add("May");
    nameList.add("Paul");
    nameList.add("Peter");
    
    System.out.println("ARRAY - NORMAL FOR LOOP");
    for (int i=0; i<nameArray.length; i++) {
      System.out.println(nameArray[i]);
    }
    
    System.out.println("ARRAYLIST - NORMAL FOR LOOP");
    for (int i=0; i<nameList.size(); i++) {
      System.out.println(nameList.get(i));
    }
    
    System.out.println("ARRAY - ENHANCED FOR LOOP");
    for (String name : nameArray) {
      System.out.println(name);
    }
       
    System.out.println("ARRAYLIST - ENHANCED FOR LOOP");
    for (String name : nameList) {
      System.out.println(name);
    }
    
  }
  
}
