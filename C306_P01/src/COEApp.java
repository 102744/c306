import java.util.ArrayList;

public class COEApp {

	public Bid findBidWithHighestPremium(ArrayList<Bid> bids) {
		// TODO: P01 Task 4 - Calculate and return the Bid with Highest COE Premium
		Bid bidderHigh = bids.get(0);
		for (Bid lists : bids){
			if (bidderHigh.getPremium() <= lists.getPremium()){
				bidderHigh = lists;
			}
		}
		return bidderHigh;
	}

	public Bid findBidWithLowestPremium(ArrayList<Bid> bids) {
		// TODO: P01 Task 5 - Calculate and return the Bid with Lowest COE Premium
		Bid bidderLow = bids.get(0);
		for (Bid lists : bids){
			if (bidderLow.getPremium() >= lists.getPremium()){
				bidderLow = lists;
			}
		}
		return bidderLow;
	}

	public double calcTotalPremium(ArrayList<Bid> bids) {
		// TODO: P01 Task 6 - Calculate and return Total COE Premium
		double total =0;
		for (Bid lists : bids){
			total += lists.getPremium();
		}
		return total;
	}

	public Bid findBid(ArrayList<Bid> bids, int year, String month, int round) {
		// TODO: P01 Task 7 - Find and return Bid for specified info
		Bid finder = bids.get(0);
		for (Bid lists: bids){
			if (lists.isThisBid(year, month, round)){
				finder = lists;
				break;
			}
		}
		return finder;
	}

/*	public Bid findGreatestDifferenceInPremium(ArrayList<Bid> bids) {
		Bid bidderDiff = bids.get(0);
		double difference = 0;
		for (Bid lists : bids){
			if (bidderDiff.getMonth() == lists.getMonth()){
				if(bidderDiff.getPremium() > lists.getPremium()){
					difference = bidderDiff.getPremium() -  lists.getPremium();
				}
				else{
					difference = lists.getPremium() -  bidderDiff.getPremium();
				}
						 
			}
			
		return bidderDiff;
	}
*/
	public String findPre(ArrayList<Bid> bids, double premium) {
		int counter = 0;
		ArrayList<String> listMon = new ArrayList<String>();
		for (Bid lists: bids){
			if (lists.getPremium() > premium){
				listMon.add(lists.getMonth());
				counter ++;
			}
		}
		String result = "There are "+ counter + " month(s) that are more than " + premium +" . \n" +listMon;
		return result;
	}
	
	public ArrayList<Bid> filterBidsByMonth(ArrayList<Bid> bids, String month) {
		
		return null;
	}
	
	public String findHighestBidder(ArrayList<Bid> bids) {
		//Enhanced finder highest bidder
		Bid HighBids = bids.get(0);
		for (Bid lists : bids){
			if (HighBids.getBidders() <= lists.getBidders()){
				HighBids = lists;
			}
		}
		return HighBids.getMonth();
	}
}
