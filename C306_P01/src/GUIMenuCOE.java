import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.TitledBorder;

public class GUIMenuCOE {

	private static String[] menuItem = { "Display Average Premium",
			"Display Highest/Lowest Premium",
			"Display Highest Difference between Two Consecutive Rounds",
			"Display Month with the highest bidders",
			"Display Premium for Specified Year, Month and Round",
			"Display Month for Specified Premium",};

	private static ArrayList<Bid> list;
	private static COEApp app;

	private static void init() {
		CsvFileReader cfr = new CsvFileReader();
		list = cfr.loadBids("coe.csv");

		app = new COEApp();
	}

	private static void displayHighestPremium() {
		Bid maxBid = app.findBidWithHighestPremium(list);

		GUIMenuCOE.output("D I S P L A Y   H I G H E S T   C O E   P R E M I U M");
		if (maxBid == null) {
			GUIMenuCOE.output("No Max Bid");
		} else {
			GUIMenuCOE.output(maxBid.toString());
		}
		GUIMenuCOE.output(" ");
	}

	private static void displayLowestPremium() {
		Bid bids = app.findBidWithLowestPremium(list);
		GUIMenuCOE.output("D I S P L A Y   L O W E S T   C O E   P R E M I U M");

		// TODO: P01 Task 8A - Find and display Bid with lowest premium
		if (bids == null) {
			GUIMenuCOE.output("No Max Bid");
		} else {
			GUIMenuCOE.output(bids.toString());
		}
		GUIMenuCOE.output(" ");
	}

	private static void displayPremium(int year, String month, int round) {
		Bid bid = app.findBid(list, year, month, round);

		GUIMenuCOE.output("D I S P L A Y   P R E M I U M");
		if (bid == null) {
			GUIMenuCOE.output("No Bid found.");
		} else {
			GUIMenuCOE.output(bid.toString());
		}
		GUIMenuCOE.output(" ");
	}

	private static void displayAveragePremium() {
		
		double averagePremium= app.calcTotalPremium(list)/list.size();

		// TODO: P01 Task 9 - Find and display average premium
		
		GUIMenuCOE.output("D I S P L A Y   A V E R A G E   C O E   P R E M I U M");
		GUIMenuCOE.output("Average COE Premium is " + averagePremium);
		GUIMenuCOE.output(" ");
	}
	
	private static void displayHighestBidder() {
		String bids = app.findHighestBidder(list);
		GUIMenuCOE.output("D I S P L A Y   H I G H E S T   B I D D E R S");

		//FINDING HIGHEST BIDDERS
		if (bids == null) {
			GUIMenuCOE.output("No Max Bid");
		} else {
			GUIMenuCOE.output("Highest Month of Bidder : " +bids.toString());
		}
		GUIMenuCOE.output(" ");
	}

/*	private static void displayHighestDifference() {
		Bid bids = app.findGreatestDifferenceInPremium(list);
		GUIMenuCOE.output("D i s p l a y   H i g h e s t   D i f f e r e n c e   B e t w e e n   T w o   C o n s e c u t i v e   R o u n d s");

		//FINDING HIGHEST DIFFERENCE 
		if (bids == null) {
			GUIMenuCOE.output("No Max Bid");
		} else {
			GUIMenuCOE.output("Highest Month of Bidder : " +bids.toString());
		}
		GUIMenuCOE.output(" ");
	}*/
	
	private static void displayPremiumWith(double premium) {
		String bid = app.findPre(list, premium);

		GUIMenuCOE.output("D I S P L A Y   S P E C I F I C   P R E M I U M");
		if (bid == null) {
			GUIMenuCOE.output("No Bid found.");
		} else {
			GUIMenuCOE.output(bid.toString());
		}
		GUIMenuCOE.output(" ");
	}
	
	private static void doOption(int choice) {
		switch (choice) {
		case 0:
			displayAveragePremium();
			break;

		case 1:
			displayHighestPremium();
			// TODO: P01 Task 8B - Call displayLowestPremium
			displayLowestPremium();
			break;
		
		case 2:
			//displayHighestDifference();
			break;
		
		case 3:
			displayHighestBidder();
			break;
		
		case 4:
			int year = GUIKeyboard.readInt("Enter Year ");
			String month = GUIKeyboard.readString("Enter Month ");
			int round = GUIKeyboard.readInt("Enter Round ");
			displayPremium(year, month, round);
			break;
		
		case 5:
			double premium = GUIKeyboard.readDouble("Enter Premium ");
			displayPremiumWith(premium);
			break;
		}
	}

	public static void main(String[] args) {
		GUIMenuCOE.showMenu("COE Application");
	}

	//
	// DO NOT CHANGE ANY CODE FROM THIS POINT ONWARDS
	// UNLESS YOU UNDERSTAND WHAT YOU ARE DOING
	//
	private static JFrame win;
	private static DefaultListModel listModel = new DefaultListModel();
	private static JList jlist = new JList(listModel);

	public static void showMenu(String title) {
		win = new JFrame(title);
		win.setBounds(100, 100, 600, 400);

		// Create the Menu Option Buttons
		JPanel p1 = new JPanel(new GridLayout(menuItem.length, 1, 5, 5));
		p1.setBorder(new TitledBorder("Menu Items"));
		for (int i = 0; i < menuItem.length; i++) {
			JButton btn = new JButton(menuItem[i]);
			btn.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					String command = e.getActionCommand();
					for (int i = 0; i < menuItem.length; i++) {
						if (command.equals(menuItem[i])) {
							doOption(i);
							break;
						}
					}
				}
			});
			p1.add(btn);
		}
		win.add(p1, BorderLayout.NORTH);

		// Create the Display Area
		JPanel p2 = new JPanel(new BorderLayout());
		jlist.setFont(new Font("Courier New", Font.PLAIN, 14));
		JScrollPane scrollPane = new JScrollPane(jlist);
		p2.setBorder(new TitledBorder("Display Area"));
		p2.add(scrollPane, BorderLayout.CENTER);
		jlist.setFont(new Font("Courier New", Font.PLAIN, 14));

		JPanel p3 = new JPanel(new FlowLayout());
		JButton btnClear = new JButton("Clear");
		btnClear.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				listModel.clear();
			}
		});

		p3.add(btnClear);
		p2.add(p3, BorderLayout.SOUTH);
		win.add(p2, BorderLayout.CENTER);

		win.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		win.setVisible(true);

		init();
	}

	public static void output(String line) {
		String[] lines = line.split("\n");
		for (int i = 0; i < lines.length; i++) {
			listModel.addElement(lines[i]);
		}
		jlist.setSelectedIndex(listModel.size() - 1);
		jlist.ensureIndexIsVisible(listModel.size() - 1);
	}
}
