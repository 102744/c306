import static org.junit.Assert.*;

import org.junit.Test;


public class BidTest {

	@Test
	public void testBid() {
		Bid bid = new Bid("Open", 2011, "Feb", 2, 20, 200, 100);

		assertTrue(bid.isThisBid(2011, "Feb", 2));
	}

}
