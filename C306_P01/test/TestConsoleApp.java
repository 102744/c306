
public class TestConsoleApp {
	public static void main(String[] args){
		String name = GUIKeyboard.readString("Enter your name");
		GUIKeyboard.display("Hello " + name);
		
		int year = GUIKeyboard.readInt("Enter the year you were borned");
		GUIKeyboard.display("Your age is " + (2012-year));
		
		boolean likeMovie= GUIKeyboard.readBoolean("Do you like movies");
		if(likeMovie){
			double spending = GUIKeyboard.readDouble("How much you spend on movies");
			GUIKeyboard.display("Your spending is " + spending);
			}
		GUIKeyboard.display("End of Survey.\nThank you very much!");
	}
}
