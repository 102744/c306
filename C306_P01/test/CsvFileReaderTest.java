import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Scanner;

import static org.hamcrest.CoreMatchers.is;
import org.junit.Before;
import org.junit.Test;


public class CsvFileReaderTest {
	private CsvFileReader app;

	@Before
	public void setup() {
		app = new CsvFileReader();
	}

	@Test
	public void testCreateBid() {
		String cols[] = {
			"2011",
			"Oct",
			"2",
			"5000.50",
			"200",
			"100"
		};

		Bid result = app.createBid(cols);

		assertThat(result.getYear(), is(2011));
		assertThat(result.getMonth(), is("Oct"));
		assertThat(result.getRound(), is(2));
		assertThat(result.getPremium(), is(5000.50));
		assertThat(result.getQuota(), is(200));
		assertThat(result.getBidders(), is(100));
	}

	@Test
	public void testExtractBids() {
		String value = "2011,Oct,2,5000.50,200,100\n2011,Nov,1,3000,100,50";
		Scanner scanner = new Scanner(value);

		ArrayList<Bid> result = app.extractBids(scanner);

		assertThat(result.size(), is(2));
	}
}
