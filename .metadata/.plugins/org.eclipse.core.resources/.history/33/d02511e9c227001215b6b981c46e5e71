
public class Sudoku {
	public static final int SIZE = 9;

	private int grid[][];

	public Sudoku() {
		grid = new int[SIZE][SIZE];
	}

	public String getValue(int row, int col) {
		if (row >= SIZE || row < 0 || col >= SIZE || col < 0) {
			throw new IllegalArgumentException();
		}	

		int value = grid[row][col];
		if (value == 0) {
			return "?";
		}
		return Integer.toString(value);
	}

	public void setValue(int value, int row, int col) {
		if (row >= SIZE || row < 0 || col >= SIZE || col < 0) {
			throw new IllegalArgumentException();
		}
		grid[row][col] = value;
	}

	public boolean isArrayValid(int array[]) {
		int[] counter = countNumbers(array);

		return checkIfAllNumbersAreOne(counter);
	}

	private boolean checkIfAllNumbersAreOne(int[] counter) {
		boolean result = true;
		for (int i = 0; i < counter.length; i++) {
			int j = counter[i];

			if (j != 1) {
				result = false;
				break;
			}
		}

		return result;
	}

	private int[] countNumbers(int[] array) {
		int[] counter = { 0, 0, 0, 0, 0, 0, 0, 0, 0 };

		for (int i = 0; i < array.length; i++) {
			if (array[i] != 0) {
				int j = array[i] - 1;

				counter[j] += 1;
			}
		}
		return counter;
	}

	public boolean isValid() {
		boolean result = isAllRowsValid();

		if (result) {
			result = isAllColsValid();
		}

		return result;
	}

	protected boolean isAllColsValid() {
		boolean result = true;

		for (int col = 0; col < SIZE; col++) {
			int[] tmp = extractCol(grid, col);

			if (!isArrayValid(tmp)) {
				result = false;
				break;
			}
		}
		return result;
	}

	protected int[] extractCol(int[][] array, int col) {
		int[] tmp = new int[SIZE];

		for (int row = 0; row < SIZE; row++) {
			int num = array[row][col];
			tmp[row] = num;
		}

		return tmp;
	}

	protected boolean isAllRowsValid() {
		boolean result = true;

		for (int i = 0; i < SIZE; i++) {
			if (!isArrayValid(grid[i])) {
				result = false;
				break;
			}
		}
		return result;
	}

	@Override
	public String toString() {
		StringBuilder result = new StringBuilder();
		for (int i = 0; i < grid.length; i++) {
			for (int j = 0; j < grid[i].length; j++) {
				result.append(grid[i][j] + " ");
			}
			result.append("\n");
		}

		return result.toString();
	}
}
