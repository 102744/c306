
public class FourInLineBoard {

	public static final int ROWS = 6;
	public static final int COLS = 7;
	private Move[][] board;

	public FourInLineBoard() {
		board = new Move[ROWS][COLS];
		newGame();
	}

	public void newGame() {
		// TODO: P02 Task 1 - Use nested For loops to initialize the board to
		// all EMPTY}
		for (int i = 0; i <ROWS; i++) {
			for (int j = 0; j < COLS; j++) {
				board[i][j] =Move.EMPTY;
			}
		}
	}


	public void placeMove(int column, Move player) {
		// TODO: P02 Task 2 - Place a disc in a specified column
		// for (.......) {
		// if (........) {
		// board[?][column] = disc;
		// break;
		// }
		// }

		for(int i = board.length-1; i>=0; i--){
			if(board[i][column]== Move.EMPTY){
				board[i][column]=player;
				break;
			}
		}
	}

	public Move seeMove(int row, int column) {
		return board[row][column];
	}

	public boolean checkWin(Move player) {
		boolean result = false;
		if (checkVertical(player) || checkHorizontal(player)
				|| checkBackwardDiagonal(player)
				|| checkForwardDiagonal(player)) {
			result = true;
		}
		return result;
	}

	public boolean checkVertical(Move player) {
		for (int r = 0; r < ROWS - 3; r++) {
			for (int c = 0; c < COLS; c++) {
				if ((board[r][c] == player) && (board[r + 1][c] == player)
						&& (board[r + 2][c] == player)
						&& (board[r + 3][c] == player)) {
					return true;
				}
			}
		}
		return false;
	}

	public boolean checkHorizontal(Move player) {
		// TODO: P02 Task 3 - Check Winning Condition for 4 discs horizontally
		for (int i = 0; i < ROWS; i++){
			for (int j =0; j< COLS-3; j++){
				if((board[i][j] == player) && (board[i][j+1]== player)
						&& (board[i][j+2]==player) && (board[i][j+3] ==player)){
					return true;
				}
			}
		}
		return false;
	}

	public boolean checkForwardDiagonal(Move player) {
		// TODO: P02 Task 4 - Check Winning Condition for 4 discs forward
		// diagonally
		for (int i = 0; i < ROWS; i++){
			for (int j =0; j< COLS-3; j++){
				if((board[i][j] == player) && (board[i-1][j+1]== player)
						&& (board[i-2][j+2]==player) && (board[i-3][j+3] ==player)){
					return true;
				}
			}
		}
		return false;
	}

	public boolean checkBackwardDiagonal(Move player) {
		// TODO: P02 Task 5 - Check Winning Condition for 4 discs backward
		// diagonally
		for (int i = 0; i < ROWS-3; i++){
			for (int j =0; j< COLS-3; j++){
				if((board[i][j] == player) && (board[i+1][j+1]== player)
						&& (board[i+2][j+2]==player) && (board[i+3][j+3] ==player)){
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		for (int r = 0; r < ROWS; r++) {
			for (int c = 0; c < COLS; c++) {
				builder.append(board[r][c]);
			}
			builder.append("\n");
		}
		return builder.toString();
	}
}
