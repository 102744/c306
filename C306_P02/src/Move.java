public enum Move {
	EMPTY(". "), RED("R "), YELLOW("Y ");

	private String str;

	private Move(String s) {
		this.str = s;
	}

	@Override
	public String toString() {
		return str;
	}
}
