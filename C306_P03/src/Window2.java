import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class Window2 {
	public Window2(){
		JFrame frame = new JFrame("Window 2");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JLabel label = new JLabel ("Name : ");
		JTextField text = new JTextField(10);
		JButton button = new JButton("Hello");
		
		frame.setLayout(new FlowLayout(FlowLayout.LEFT));
		
		frame.add(label);
		frame.add(text);
		frame.add(button);
		
		frame.setLocationRelativeTo(null);
		frame.pack();
		frame.setVisible(true);
	}

}
