import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;


public class GUIMysteryNumber {
	private MysteryNumber number;
	private JTextField Num = new JTextField(10);
	private JLabel top, hint,count;
	private int a =1;

	public GUIMysteryNumber(MysteryNumber number){
		this.number = number;
		container();
	}
	public void container(){
		JFrame frame = new JFrame("Number Guessing Game");
		frame.setBounds(50, 50, 340, 200);

		frame.add(topPanel(), BorderLayout.NORTH);
		frame.add(leftPanel(), BorderLayout.WEST);
		frame.add(resultGuess(),BorderLayout.CENTER);
		frame.add(bottom(), BorderLayout.SOUTH);
		frame.setVisible(true);



	}

	private JPanel resultGuess(){
		JPanel result = new JPanel();
		top = new JLabel ("Start Guessing!!", SwingConstants.CENTER);
		result.add(top,BorderLayout.CENTER);
		hint = new JLabel ("",SwingConstants.CENTER);
		result.add(hint,BorderLayout.SOUTH);
		count = new JLabel ("",SwingConstants.CENTER);
		result.add(count,BorderLayout.SOUTH);
		return result;
	}

	private JLabel topPanel(){
		JLabel topPan = new JLabel("Guess a number between 1 to 49",SwingConstants.CENTER);
		return topPan;
	}

	private JPanel leftPanel(){
		JPanel left = new JPanel(new FlowLayout());
		left.add(new JLabel("Please "));
		left.add(numberGuess());
		return left;
	}

	private JPanel bottom(){
		JPanel bottom = new JPanel(new FlowLayout());
		bottom.add(numberGen());
		bottom.add(Num);
		return bottom;
	}

	private JButton numberGen(){
		JButton generate = new JButton("Generate Number");
		generate.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				generate();
			}
		});
		return generate;
	}
	private JButton numberGuess(){
		JButton guess = new JButton("Guess");

		guess.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				resultNum();
			}
		});
		return guess;

	}

	private void generate(){
		Num.setText(null);
		number = new MysteryNumber();
	}


	private void resultNum(){
		
		int num = Integer.parseInt(Num.getText());
		top.setText(number.getResult(num).toString());
		hint.setText(number.getHint(num));
		if (top.getText().equals("HIGH") || top.getText().equals("LOW") ){
			a++;
		}
		
		else if (number.getResult(num).toString().equals("EQUAL")){
			String countResult = "You guessed for " + a + "times!";
			count.setText(countResult);

		}
	}
		public static void main(String[] args){
			new GUIMysteryNumber(new MysteryNumber());
		}


	}
