import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
public class Window3 {
	private JTextField text = new JTextField(10);
	
	public Window3(){
		JFrame frame = new JFrame("Window 3");
		frame.setBounds(50, 50, 400, 100);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JLabel label = new JLabel("Name : ");
		JButton button = new JButton("Hello");
		
		JPanel panel = new JPanel();
		panel.setBorder(BorderFactory.createTitledBorder("Personal"));
		panel.setLayout(new FlowLayout(FlowLayout.LEFT));
		
		panel.add(label);
		panel.add(text);
		panel.add(button);
		
		frame.add(panel);
		
		button.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				sayHello();
			}
		});
		frame.setLocationRelativeTo(null);
		frame.pack();
		frame.setVisible(true);
	}
	private void sayHello(){
		String mess = "Hi " + text.getText();
		JOptionPane.showMessageDialog(null, mess);
	}
	
}
