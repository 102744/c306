import java.util.Random;

public class MysteryNumber {
	private int mysteryNumber;

	public MysteryNumber() {
		mysteryNumber = new Random().nextInt(50);
	}

	public int getNumber() {
		return mysteryNumber;
	}

	public Result getResult(int guessNumber) {
		if (guessNumber == mysteryNumber) {
			return Result.EQUAL;
		} else if (guessNumber > mysteryNumber) {
			return Result.HIGH;
		} else {
			return Result.LOW;
		}
	}

	public String getHint(int guessNumber) {
	//	int a[] = {guessNumber};
		if (guessNumber == mysteryNumber) {
			return "";
		} else if (guessNumber > mysteryNumber) {
			return "The number is between 0 and " + (guessNumber - 1);
		} else {
			return "The number is between " + (guessNumber + 1) + " and 49";
		}
	}
}
