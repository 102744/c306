import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;


public class MysteryNumberTest {
	private MysteryNumber mystery;
	private int num;

	@Before
	public void setup() {
		mystery = new MysteryNumber();
		num = mystery.getNumber();
	}

	@Test
	public void testCorrectGuess() {
		assertThat(mystery.getResult(num), is(Result.EQUAL));
		assertThat(mystery.getHint(num), is(""));
	}

	@Test
	public void testHigherGuess() {
		int guess = num + 9;

		assertThat(mystery.getResult(guess), is(Result.HIGH));
		assertThat(mystery.getHint(guess), is("The number is between 0 and " + (guess - 1)));
	}

	@Test
	public void testLowerGuess() {
		int guess = num - 9;
		System.out.println(guess);
		assertThat(mystery.getResult(guess), is(Result.LOW));
		assertThat(mystery.getHint(guess), is("The number is between " + (guess + 1) + " and 49"));
	}
}
