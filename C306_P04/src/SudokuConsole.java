import java.util.Scanner;

public class SudokuConsole {
	public static void main(String[] args) {
		Sudoku game = new Sudoku();

		while (!game.isValid()) {
			System.out.println(game);
			if (game.isValid()) {
				System.out.println("Sudoku completed");
			} else {
				System.out.println("Sudoku incomplete");
			}
			int row = readInt("Enter row:");
			int col = readInt("Enter col:");
			int val = readInt("Enter val:");

			game.setValue(val, row, col);
		}
	}

	public static int readInt(String p) {
		System.out.print(p);
		try {
			return Integer.parseInt(new Scanner(System.in).nextLine());
		} catch (NumberFormatException nfe) {
			System.out.println("Please enter an integer");
			return readInt(p);
		}
	}
}
