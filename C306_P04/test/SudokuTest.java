import static org.junit.Assert.assertThat;
import static org.hamcrest.CoreMatchers.is;

import org.junit.Before;
import org.junit.Test;


public class SudokuTest {
	private Sudoku game;

	@Before
	public void setup() {
		game = new Sudoku();
	}

	@Test
	public void testStartGameAllCellQuestionMark() {
		for (int i = 0; i < Sudoku.SIZE; i++) {
			for (int j = 0; j < Sudoku.SIZE; j++) {
				assertThat(game.getValue(i, j), is("?"));
			}
		}
	}

	@Test
	public void testStartGameNotValid() {
		assertThat(game.isValid(), is(false));
	}

	@Test
	public void testSetValue() {
		game.setValue(2, 1, 3);

		for (int i = 0; i < Sudoku.SIZE; i++) {
			for (int j = 0; j < Sudoku.SIZE; j++) {
				if (i == 1 && j == 3) {
					assertThat(game.getValue(i, j), is("2"));
				} else {
					assertThat(game.getValue(i, j), is("?"));
				}
			}
		}
	}

	@Test
	public void testSimplePass() {
		int[] array = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		boolean result = game.isArrayValid(array);

		assertThat(result, is(true));
	}

	@Test
	public void testSimpleFail() {
		int[] array = { 1, 1, 1, 1, 1, 1, 1, 1, 1 };
		boolean result = game.isArrayValid(array);

		assertThat(result, is(false));
	}

	@Test
	public void testValidWrong() {
		int[][] sample = {
			{ 1, 2, 3, 4, 5, 6, 7, 8, 9 },
			{ 1, 2, 3, 4, 5, 6, 7, 8, 9 },
			{ 1, 2, 3, 4, 5, 6, 7, 8, 9 },
			{ 1, 2, 3, 4, 5, 6, 7, 8, 9 },
			{ 1, 2, 3, 4, 5, 6, 7, 8, 9 },
			{ 1, 2, 3, 4, 5, 6, 7, 8, 9 },
			{ 1, 2, 3, 4, 5, 6, 7, 8, 9 },
			{ 1, 2, 3, 4, 5, 6, 7, 8, 9 },
			{ 1, 2, 3, 4, 5, 6, 7, 8, 9 }
		};

		for (int i = 0; i < Sudoku.SIZE; i++) {
			for (int j = 0; j < Sudoku.SIZE; j++) {
				game.setValue(sample[i][j], i, j);
			}
		}

		assertThat(game.isValid(), is(false));
	}

	@Test
	public void testValidRight() {
		int[][] solution = {
			{ 5, 3, 4, 6, 7, 8, 9, 1, 2 },
			{ 6, 7, 2, 1, 9, 5, 3, 4, 8 },
			{ 1, 9, 8, 3, 4, 2, 5, 6, 7 },
			{ 8, 5, 9, 7, 6, 1, 4, 2, 3 },
			{ 4, 2, 6, 8, 5, 3, 7, 9, 1 },
			{ 7, 1, 3, 9, 2, 4, 8, 5, 6 },
			{ 9, 6, 1, 5, 3, 7, 2, 8, 4 },
			{ 2, 8, 7, 4, 1, 9, 6, 3, 5 },
			{ 3, 4, 5, 2, 8, 6, 1, 7, 9 },
		};

		for (int i = 0; i < Sudoku.SIZE; i++) {
			for (int j = 0; j < Sudoku.SIZE; j++) {
				game.setValue(solution[i][j], i, j);
			}
		}

		assertThat(game.isValid(), is(true));
	}
}
